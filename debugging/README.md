## Debugging Commands
**tkn pipelinerun ls**

**tkn pipelinerun logs failing-run-w5ccn**

**kubectl get pods**

>  failing-run-w5ccn-fail-xqvdv-pod-bkh6j   0/1  ImagePullBackOff   0  4m44s

**kubectl describe pod failing-run-w5ccn-fail-xqvdv-pod-bkh6j**

## Exit Codes
0 - means success, any other code means failure
exit $(params.exitcode)