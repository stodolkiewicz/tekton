# Intro
A task consists of 1 or more steps.  
A task runs in a pod.  
A step runs in a container, which in turn is in the pod.  

# TaskRun
kubectl get taskrun array-params-run-4mk44 -o yaml