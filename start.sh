minikube start
kubectl apply -f https://storage.googleapis.com/tekton-releases/pipeline/previous/v0.20.1/release.yaml
kubectl apply --filename https://github.com/tektoncd/dashboard/releases/latest/download/tekton-dashboard-release.yaml  
kubectl --namespace tekton-pipelines port-forward svc/tekton-dashboard 9097:9097 