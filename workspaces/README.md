## Introduction
**results** - can share small bits of data across tasks, or withing a single task context

**WORKSPACES** - SHARE LOTS OF DATA across tasks, or withing a single task context
data in workspaces can be persisted FOREVER or cleaned up after pipelineRun (finally task)

## Volume Sources
Workspaces need some sort of volume to mount the files to be shared.
1. emptyDir - should only be used in a context of a task (no inter-task communication)
2. ConfigMap - after mounting the configMap, configurations are available, in read-only mode, as files in the mountedVolume.
3. Secret - similar to ConfigMap, but uses Secret
4. PVC and volumeClaimTemplate  
  
bothused to share a folder across task as a **writable** file system  
    
**PVC**  
    Must exist somewhere before running pipeline. When you use it, you can **run 1 pipelineRun at the same time** (because it uses the PVC!). You may need to use **finally** task to clean everything once you are done,

**volumeClaimTemplate**  
    PVC will be created and deleted once the run is deleted. Plus you can run **multiple pipelineRuns** at the same time.
  
#### --- Pass parameters in tkn ---
```
tkn task start clone-and-list **-w name=source,emptyDir=""** --showlog
```

#### --- TaskRun's generateName ---
```
apiVersion: tekton.dev/v1beta1
kind: TaskRun
metadata:
  generateName: git-clone-tr
```
If you used name, you would have to change it for each start of TaskRun.

#### --- Create TaskRun ---
kubectl **create** -f clone-and-list-tr.yaml

#### --- in-a-pipeline ---
stodo@dawid:~/Desktop/tekton/workspaces/persisting-data-within-pipeline$ tkn pipeline start clone-and-list --showlog  
Please give specifications for the workspace: codebase   
? Name for the workspace : codebase  
? Value of the Sub Path :    
? Type of the Workspace : pvc  
? Value of Claim Name : tekton-pvc  
PipelineRun started: clone-and-list-run-kdtrt  
Waiting for logs to be available...  
[clone : clone] Cloning into './source'...  
[clone : clone] POST git-upload-pack (175 bytes)  
[clone : clone] POST git-upload-pack (467 bytes)  
  
[list : list] README.md  
[list : list] app  
[list : list] demo  
[list : list] installation  

#### To get to pod's /bin/bash
kubectl exec --stdin --tty task-pv-pod -- /bin/bash