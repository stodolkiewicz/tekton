# Dependencies
stodo@dawid:~/Desktop/tekton/param$ tkn version  
Client version: 0.17.2  
Pipeline version: v0.20.1  
Dashboard version: v0.29.2  

# Installation (Ubuntu)
Assuming, you already have installed:
 - minikube
 - kubectl

## 1. Client
Installation process
1. Go to releases
https://github.com/tektoncd/cli/releases

2. Click on one (preferably 0.17.2) and download a *64bit.deb file

3. Execute
```
sudo dpkg -i ./[downloaded *64bit.deb file]
```

https://tekton.dev/docs/cli/ - instructions for all operating systems

## 2. Pipeline 
kubectl apply -f https://storage.googleapis.com/tekton-releases/pipeline/previous/v0.20.1/release.yaml

## 3. Dashboard 
kubectl apply --filename https://github.com/tektoncd/dashboard/releases/latest/download/tekton-dashboard-release.yaml  

kubectl --namespace tekton-pipelines port-forward svc/tekton-dashboard 9097:9097 

Now, try out: http://localhost:9097/

# How to run it
<b> To create a task: </b>  
kubectl apply -f hello.yaml

<b>To run this task</b>  
tkn task start hello --showlog

# Notes to self
To loop over a Task, use TaskLoop!
https://gist.github.com/hokadiri/154a4d1b8be5e8e234845adb34bbc010
